import path from 'path';

import {
  app,
  BrowserWindow,
  crashReporter
} from 'electron';

import Menu from 'menu';
import template from './menu';

crashReporter.start();

let mainWindow = null;

class Boot {
  constructor() {
    this.setBrowserWindow();
  }
  setBrowserWindow() {
    app.on('window-all-closed', () => {
      if (process.platform !== 'darwin') {
        app.quit();
      }
    });

    app.on('ready', () => {
      mainWindow = new BrowserWindow({width: 1024, height: 768});

      mainWindow.loadURL(path.join('file://', __dirname, '/dist/index.html'));

      if (process.env.NODE_ENV === 'develop') {
        mainWindow.webContents.openDevTools();
      }

      mainWindow.on('closed', () => {
        mainWindow = null;
      });

      const menu = Menu.buildFromTemplate(template);
      Menu.setApplicationMenu(menu);
    });
  }
}

const boot = new Boot();
export default boot;
