import {
  app
} from 'electron';

import Menu from 'menu';
import packageJson from '../package.json';

const template = [
  {
    label: packageJson.name,
    submenu: [
      {
        label: '終了',
        accelerator: 'Command+Q',
        click: function() {
          app.quit();
        }
      }
    ]
  },
  {
    label: '編集',
    submenu: [
      {
        label: '戻る',
        accelerator: 'CmdOrCtrl+Z',
        selector: 'undo:'
      },
      {
        label: '進む',
        accelerator: 'Shift+CmdOrCtrl+Z',
        selector: 'redo:'
      },
      {
        type: 'separator'
      },
      {
        label: 'カット',
        accelerator: 'CmdOrCtrl+X',
        selector: 'cut:'
      },
      {
        label: 'コピー',
        accelerator: 'CmdOrCtrl+C',
        selector: 'copy:'
      },
      {
        label: '貼り付け',
        accelerator: 'CmdOrCtrl+V',
        selector: 'paste:'
      },
      {
        label: '全て選択',
        accelerator: 'CmdOrCtrl+A',
        selector: 'selectAll:'
      }
    ]
  }
];
