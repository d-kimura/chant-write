import { combineReducers } from 'redux';

import app from './App';
import editor from './Editor';

const reducers = combineReducers({
  app,
  editor
});

export default reducers;
