import {
  CHANGE_TEXT,
  RECEIVE_TEXT_LINT
} from '../constants/ActionTypes';

const initialState = {
  markdown: '',
  error: []
};

/**
 * [editor description]
 * @method editor
 * @param  {[type]} state  =             initialState [description]
 * @param  {[type]} action [description]
 * @return {[type]}        [description]
 */
export default function editor(state = initialState, action) {
  switch (action.type) {
    case CHANGE_TEXT:
      return Object.assign({}, state, {
        markdown: action.markdown
      });
    case RECEIVE_TEXT_LINT:
      return Object.assign({}, state, {
        markdown: action.markdown,
        error: action.error
      });
    default:
      return state;
  }
}
