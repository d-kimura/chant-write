import {
  REQUEST_TEXT_LINT,
  RECEIVE_TEXT_LINT
} from '../constants/ActionTypes';

const initialState = {
  isFetch: false
};

/**
 * [app description]
 * @method app
 * @param  {[type]} state  =             initialState [description]
 * @param  {[type]} action [description]
 * @return {[type]}        [description]
 */
export default function app(state = initialState, action) {
  switch (action.type) {
    case REQUEST_TEXT_LINT:
      return Object.assign({}, state, {
        isFetch: true
      });
    case RECEIVE_TEXT_LINT:
      return Object.assign({}, state, {
        isFetch: false
      });
    default:
      return state;
  }
}
