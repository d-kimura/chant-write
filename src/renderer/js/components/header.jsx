import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
      <header className="header navbar navbar-full navbar-dark bg-inverse">
       <div className="container-fluid">
         <h1 className="navbar-brand">Chant Write</h1>
         <div className="form-inline navbar-form pull-right">
           <button
             className="btn btn-primary btn-sm"
             onClick={ this.handleTextLint.bind(this) }
           >文章チェック</button>
         </div>
       </div>
     </header>
    );
  }
  handleTextLint(e) {
    e.preventDefault();

    const { editor, actions } = this.props;
    actions.postTextLintIfNeeded(editor);
  }
}
