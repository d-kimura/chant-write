import React, { Component } from 'react';

export default class ErrorList extends Component {
  render() {
    const { error } = this.props.editor;

    const errorList = error.map((item, index) => {
      return (
        <div key={ index } className="alert alert-danger" role="alert">
          <p>
            <strong>{ item.type }</strong>
            <span>line: { item.line } column: { item.column }</span>
          </p>
          <p>{ item.text }</p>
        </div>
      );
    });

    return (
      <div className="footer container-fluid">
        { errorList }
      </div>
    );
  }
}
