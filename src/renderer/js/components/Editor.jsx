import React, { Component } from 'react';

import AceEditor from 'react-ace';
import 'brace/mode/markdown';
import 'brace/theme/github';

import md2react from 'md2react';
const options = {
  breaks: true,
  tasklist: true
};

export default class Editor extends Component {
  render() {
    const { markdown } = this.props.editor;

    return (
      <div className="contents container-fluid">
        <div className="row">
          <div className="col col-xs-6">
            <h2 className="title">Markdown</h2>
            <div className="editor">
              <AceEditor
                mode="markdown"
                width="auto"
                theme="github"
                fontSize={ 16 }
                showPrintMargin={ false }
                highlightActiveLine={ false }
                onChange={ this.handleChangeText.bind(this) }
                value={ markdown }
              />
            </div>
          </div>
          <div className="col col-xs-6">
            <h2 className="title">Preview</h2>
            <div className="markdown-body">
              { md2react(markdown, options) }
            </div>
          </div>
        </div>
      </div>
    );
  }
  handleChangeText(value) {
    const { actions } = this.props;
    actions.changeText({
      markdown: value
    });
  }
}
