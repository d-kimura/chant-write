import '../scss/style.scss';

import React from 'react';
import { render } from 'react-dom';

import reducers from './reducers/';

import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';

const logger = createLogger();
const createStoreWithMiddleware =
  applyMiddleware(thunk, promise, logger)(createStore);

const store = createStoreWithMiddleware(reducers);

import App from './containers/App';

render(
  <Provider store={ store }>
    <App />
  </Provider>,
  document.getElementById('app')
);
