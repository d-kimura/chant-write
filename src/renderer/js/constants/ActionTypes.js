export const CHANGE_TEXT = 'CHANGE_TEXT';

export const REQUEST_TEXT_LINT = 'REQUEST_TEXT_LINT';
export const RECEIVE_TEXT_LINT = 'RECEIVE_TEXT_LINT';
