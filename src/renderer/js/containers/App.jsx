import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Header from '../components/Header';
import Editor from '../components/Editor';
import ErrorList from '../components/ErrorList';

import * as LintActions from '../actions/Editor';

class App extends Component {
  render() {
    const { editor, actions } = this.props;

    return (
      <div>
        <Header actions={ actions } editor={ editor }/>
        <Editor actions={ actions } editor={ editor }/>
        <ErrorList editor={ editor }/>
      </div>
    );
  }
}

/**
 * [mapStateToProps description]
 * @method mapStateToProps
 * @param  {[type]}        state [description]
 * @return {[type]}              [description]
 */
function mapStateToProps(state) {
  return {
    app: state.app,
    editor: state.editor
  };
}

/**
 * [mapDispatchToProps description]
 * @method mapDispatchToProps
 * @param  {[type]}           dispatch [description]
 * @return {[type]}                    [description]
 */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(LintActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
