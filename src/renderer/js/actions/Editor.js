import fetch from 'isomorphic-fetch';

import {
  CHANGE_TEXT,
  REQUEST_TEXT_LINT,
  RECEIVE_TEXT_LINT
} from '../constants/ActionTypes';

/**
 * [changeText description]
 * @method changeText
 * @param  {[type]}   options [description]
 * @return {[type]}           [description]
 */
export function changeText(options) {
  const { markdown } = options;
  return {
    type: CHANGE_TEXT,
    markdown: markdown
  };
}

/**
 * [requestTextLint description]
 * @method requestTextLint
 * @param  {[type]}        option [description]
 * @return {[type]}               [description]
 */
function requestTextLint(option) {
  const { markdown } = option;
  return {
    type: REQUEST_TEXT_LINT,
    markdown: markdown
  };
}

/**
 * [receiveTextLint description]
 * @method receiveTextLint
 * @param  {[type]}        option [description]
 * @param  {[type]}        json   [description]
 * @return {[type]}               [description]
 */
function receiveTextLint(option, json) {
  const { markdown } = option;
  const { result } = json;
  return {
    type: RECEIVE_TEXT_LINT,
    markdown: markdown,
    error: result
  };
}

/**
 * [postTextLint description]
 * @method postTextLint
 * @param  {[type]}     option [description]
 * @return {[type]}            [description]
 */
function postTextLint(option) {
  const { markdown } = option;

  return dispatch => {
    dispatch(requestTextLint(option));

    return fetch('http://text-lint-server.herokuapp.com/api/linter', {
      // credentials: 'include',
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        markdown: markdown
      })
    })
    .then(res => res.json())
    .then(res => {
      dispatch(receiveTextLint(option, res));
    });
  };
}

/**
 * [postTextLintIfNeeded description]
 * @method postTextLintIfNeeded
 * @param  {[type]}             option [description]
 * @return {[type]}                    [description]
 */
export function postTextLintIfNeeded(option) {
  return (dispatch, getState) => {
    if (getState().app.isFetch) {
      return Promise.resolve();
    } else {
      return dispatch(postTextLint(option));
    }
  };
}
