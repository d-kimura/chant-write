/* eslint-disable */
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

require('bootstrap-v4-webpack');

module.exports = {
  context: __dirname + '/src/renderer',
  entry: {
    js:   './js/index.js',
    html: './index.html'
  },
  output: {
      path: __dirname + '/dist',
      filename: './js/app.bundle.js'
  },
  module: {
    loaders: [
      {
        test:     /\.html$/,
        loaders:  ['file?name=[name].[ext]'],
      },
      {
        test:     /\.scss$/,
        loader:  ExtractTextPlugin.extract('style', '!css!sass')
      },
      {
        test:     /\.json$/,
        loader:  'json'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  },
  plugins: [
    new ExtractTextPlugin('css/style.css'),
    new webpack.ProvidePlugin({
      React: 'react'
    })
  ]
}
