const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = require('menu');
const packageJson = require('./package.json');

electron.crashReporter.start();

var mainWindow = null;

const template = [
  {
    label: packageJson.name,
    submenu: [
      {
        label: '終了',
        accelerator: 'Command+Q',
        click: function() {
          app.quit();
        }
      }
    ]
  },
  {
    label: '編集',
    submenu: [
      {
        label: '戻る',
        accelerator: 'CmdOrCtrl+Z',
        selector: 'undo:'
      },
      {
        label: '進む',
        accelerator: 'Shift+CmdOrCtrl+Z',
        selector: 'redo:'
      },
      {
        type: 'separator'
      },
      {
        label: 'カット',
        accelerator: 'CmdOrCtrl+X',
        selector: 'cut:'
      },
      {
        label: 'コピー',
        accelerator: 'CmdOrCtrl+C',
        selector: 'copy:'
      },
      {
        label: '貼り付け',
        accelerator: 'CmdOrCtrl+V',
        selector: 'paste:'
      },
      {
        label: '全て選択',
        accelerator: 'CmdOrCtrl+A',
        selector: 'selectAll:'
      }
    ]
  }
];

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', function() {
  mainWindow = new BrowserWindow({width: 1024, height: 768});

  mainWindow.loadURL('file://' + __dirname + '/dist/index.html');

  process.env.NODE_ENV === 'develop' ? mainWindow.webContents.openDevTools() : '';

  mainWindow.on('closed', function() {
    mainWindow = null;
  });

  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
});
