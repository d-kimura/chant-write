const packager = require('electron-packager');
const fs = require('fs');
const path = require('path');

const paths = {
  packageJson: path.join(__dirname, '/package.json')
};

const packageJson = JSON.parse(fs.readFileSync(paths.packageJson, 'utf-8'));

const options = {
  dir: '.',
  name: packageJson.name,
  platform: ['darwin', 'win32'],
  arch: 'all',
  version: require('electron-prebuilt/package.json').version,
  overwrite: true,
  // prune: true,
  icon: path.join(__dirname, 'assets/images/icon.icns'),
  ignore: ['node_modules', 'packages'],
  out: 'packages'
};

packager(options, (err, path) => {
  if (err) {
    return console.log(err);
  }
  console.log(path);
});
